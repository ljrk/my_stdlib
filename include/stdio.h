//#include <asm-generic/errno.h>

/* WG14/N1256 Committee Draft — Septermber 7, 2007 ISO/IEC 9899:TC3 */

/* p 262 (pdf: 274) */
typedef struct _C_FILE { /* TODO: put into seperate file, should be opacque */
	int cpos; /* current position indicator */
	int serrn; /* stream error state */
	int feofn; /* end of file indicator */

	/* private stuff (not defined by standard) */
	int fd; /* file descriptor */
	/* TODO: ... to be made complete */
} FILE;


/* use defines similar to GNU ones */
#define EOF -1

/* Description
 *  The *fputc* function writes the character specified by *c* (converted to an
 *  *unsigned char*) to the output stream pointed to by *stream*, at the
 *  position indicated by the associated file position indicator for the stream
 *  (if defined), and advances the indicator appropriately. If the file cannot
 *  support positioning requests, or if the stream was opened with append mode,
 *  the character is appended to the output stream.
 * Returns
 *  The *fputc* function returns the character written. If a write error occurs,
 *  the error indicator for the stream is set and *fputc* returns *EOF*.
 */
int fputc(int c, FILE *stream); //TODO
/* Description
 *  The *putc* function is equivalent to *fputc*, except that if it is
 *  implemented as a macro, it may evaluate *stream* more than once, so that
 *  argument should never be an expression with side effects.
 * Returns
 *  The *putc* function returns the character written. If a write error occurs,
 *  the error indicator for the stream is set and putc returns *EOF*.
 */
int putc(int c, FILE *stream); //TODO
/* Description
 *  The *putchar* function is equivalent to *putc* with the second argument
 *  *stdout*.
 * Returns
 *  The *putchar* function returns the character written. If a write error
 *  occurs, the error indicator for the stream is set and *putchar* returns
 *  *EOF*.
 */
int putchar(int c); //TODO

/* Description
 *  The *puts* function writes the string pointed to by *s* to the stream
 *  pointed to by *stdout*, and appends a new-line character to the output. The
 *  terminating null character is not written.
 * Returns
 *  The *puts* function returns *EOF* if a write error occurs; otherwise it
 *  returns a nonnegative value.
 */
int puts(const char *s); //TODO: fix to specification
