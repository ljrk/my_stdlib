/* taken from 64-bit glibc; should be put into stddef.h
 * ssize_t is only internal, do not expose!
 */
typedef long unsigned int size_t;
