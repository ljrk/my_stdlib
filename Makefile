export PATH := /opt/android-ndk/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/:$(PATH)
export ANDROID_NDK=/opt/android-ndk


CC=arm-linux-androideabi-gcc
CFLAGS=-static -nostdlib -fno-builtin -march=armv5te -mfloat-abi=soft -mfpu=vfp -mtls-dialect=gnu

BDIR=./bin
ODIR=${BDIR}/obj
ODIRASM=${BDIR}/obj_asm
SDIR=./

CCx86=gcc
CFLAGSx86=-fno-builtin -nostdlib -g3 -Og

# binaries
_TEST=test
_STDIO=puts asm_puts asm_main

# objects
_STDIOO=$(addsuffix .o, ${_STDIO})
_ARM_STDIOO=$(addprefix ./bin/arm/obj/, ${_STDIOO})
_X86_STDIOO=$(addprefix ./bin/x86/obj/, ${_STDIOO})

_TESTO=$(addsuffix .o, ${_TEST})
_ARM_TESTO=$(addprefix ./bin/arm/obj/, $(_TESTO))
_X86_TESTO=$(addprefix ./bin/x86/obj/, $(_TESTO))


default: all
all: arm x86

arm: arm_test
x86: x86_test

# executables
arm_test: ${BDIR}/arm/${_TEST}
x86_test: ${BDIR}/x86/${_TEST}

# objects
arm_stdio: ${_ARM_STDIOO}
x86_stdio: ${_X86_STDIOO}


# Compile rule
./bin/arm/obj/asm_%.o: $(SDIR)/arm/asm_%.S | prep
	$(CC) $(CFLAGS) -c -o $@ $<
./bin/x86/obj/asm_%.o: $(SDIR)/x86/asm_%.S | prep
	${CCx86} ${CFLAGSx86} -c -o $@ $<

./bin/arm/obj/%.o: $(SDIR)/%.c | prep
	$(CC) $(CFLAGS) -c -o $@ $<
./bin/x86/obj/%.o: $(SDIR)/%.c | prep
	${CCx86} ${CFLAGSx86} -c -o $@ $<



# Linking
./bin/arm/test: ${_ARM_STDIOO} ${_ARM_TESTO}
	${CC} $(CFLAGS) -o $@ $+
./bin/x86/test: ${_X86_STDIOO} ${_X86_TESTO}
	${CCx86} ${CFLAGSx86} -o $@ $+

prep:
	@mkdir -p ./bin/arm/obj
	@mkdir -p ./bin/x86/obj

clean:
	@rm -rf ${BDIR}/*

.PHONY: all clean prep test stdio
