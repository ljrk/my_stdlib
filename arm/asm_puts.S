.data

.text

.globl write 
write:
/*
	r0: int fd
	r1: char *
	r2: int len
*/
	mov	%r7, $4		@syscall print
	svc	$0

	mov pc, lr
	bx lr
